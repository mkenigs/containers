#!/bin/bash
set -euo pipefail

# Intercept the Ctrl-C signal to be able to stop the call to buildah
trap ctrl_c INT

# Make a note of the package versions.
buildah --version

# Write all image metadata in the docker format, not the standard OCI format.
# Newer versions of docker can handle the OCI format, but older versions, like
# the one shipped with Fedora 30, cannot handle the format.
export BUILDAH_FORMAT=docker

function say { echo "$@" | toilet -f mono12 -w 400 | lolcat -f || true; }
function echo_green { echo -e "\e[1;32m${1}\e[0m"; }
function echo_red { echo -e "\e[1;31m${1}\e[0m"; }
function echo_yellow { echo -e "\e[1;33m${1}\e[0m"; }

declare -i FAILED=0
declare child_pid

function ctrl_c {
    kill -9 "$child_pid"
    echo_red "Operation terminated by the user"
    exit 1
}

function loop {
    _loop 5 10 only_notify "$@"
}

function loop_or_fail {
    _loop 5 10 exit_on_fail "$@"
}

function _loop {
    local count=$1 delay=$2 exit_on_fail=$3 s
    shift 3
    for i in $(seq "$count"); do
        # we want set -e still in effect, but also get the exit code; so
        # use a background process, as set -e is kept for background
        # commands, and the exit code checking is possible with wait later
        "$@" &
        child_pid=$!
        wait $child_pid && s=0 && break || s=$? && sleep "$delay"
    done
    FAILED+=$((s > 0))
    if [ "$exit_on_fail" = "exit_on_fail" ]; then
        (exit $s)
    fi
}

# Build the container.
say "$IMAGE_NAME"

# if running in GitLab, tag with pipeline ID, otherwise latest
if [ -v CI_PIPELINE_ID ]; then
    IMAGE_PIPELINE_TAG="p-${CI_PIPELINE_ID}"
else
    IMAGE_PIPELINE_TAG="latest"
fi

# figure out a reasonable description
if [ -v CI_COMMIT_TAG ]; then
    IMAGE_DESCRIPTION="${CI_PROJECT_PATH}/${IMAGE_NAME}:${CI_COMMIT_TAG}@git-${CI_COMMIT_SHORT_SHA}"
elif [ -v CI_MERGE_REQUEST_IID ]; then
    IMAGE_DESCRIPTION="${CI_MERGE_REQUEST_SOURCE_PROJECT_PATH}:mr-${CI_MERGE_REQUEST_IID}@git-${CI_COMMIT_SHORT_SHA}"
elif [ -v CI_COMMIT_REF_NAME ]; then
    IMAGE_DESCRIPTION="${CI_PROJECT_PATH}/${IMAGE_NAME}:${CI_COMMIT_REF_NAME}@git-${CI_COMMIT_SHORT_SHA}"
else
    IMAGE_DESCRIPTION="Local build for ${IMAGE_NAME}"
fi

IMAGE_EXTRA_TAGS=()
if [ -v AUTOMATIC_TAGS ]; then
    IMAGE_EXTRA_TAGS+=("git-${CI_COMMIT_SHORT_SHA}")
    if [ -v CI_MERGE_REQUEST_IID ]; then
        IMAGE_EXTRA_TAGS+=("mr-${CI_MERGE_REQUEST_IID}")
    fi
    if [ -v CI_COMMIT_TAG ]; then
        IMAGE_EXTRA_TAGS+=("${CI_COMMIT_TAG}")
    fi
    if [ -v CI_COMMIT_REF_NAME ] && [ "${CI_COMMIT_REF_NAME}" = "${CI_DEFAULT_BRANCH}" ]; then
        IMAGE_EXTRA_TAGS+=("latest")
    fi
fi
if [ -v DEPLOY_TAG ]; then
    IMAGE_EXTRA_TAGS+=("${DEPLOY_TAG}")
fi

export CPATH=includes:/usr/local/share/cki

echo_yellow "Buildah build"

# enable qemu support; this works in the buildah image, and should cause no
# harm on a local installation where the script most likely is not installed
if [ -n "${IMAGE_ARCH:-}" ] && [ -x /usr/local/bin/qemu-binfmt-conf.sh ]; then
    qemu-binfmt-conf.sh --qemu-suffix -static --qemu-path /usr/bin --persistent yes || true
fi

export IMAGE="${IMAGE_NAME}:${IMAGE_PIPELINE_TAG}${IMAGE_ARCH:+-${IMAGE_ARCH}}"

if ! [ -v IMAGE_ARCHES ] && [ "${#IMAGE_EXTRA_TAGS[@]}" -eq 0 ]; then
    if [[ "$IMAGE_NAME" == "eln" ]]; then
        # build the Fedora ELN base image
        loop_or_fail ./build_eln_from_scratch.sh
    else
        loop_or_fail buildah bud \
            ${BASE_IMAGE+--build-arg "BASE_IMAGE=$BASE_IMAGE"} \
            ${BASE_IMAGE_TAG+--build-arg "BASE_IMAGE_TAG=$BASE_IMAGE_TAG"} \
            ${IMAGE_DESCRIPTION+--build-arg "IMAGE_DESCRIPTION=$IMAGE_DESCRIPTION"} \
            ${IMAGE_ARCH:+--arch "$IMAGE_ARCH"} \
            ${IMAGE_ARCH:+--override-arch "$IMAGE_ARCH"} \
            --file "builds/${IMAGE_NAME}".in \
            --tag "${IMAGE}" \
            .
    fi
fi

# if not running in GitLab, do not push
if ! [ -v CI_PIPELINE_ID ]; then
    exit 0
fi

if [ -v REGISTRIES ]; then
    read -r -a REGISTRIES <<< "$REGISTRIES"
else
    REGISTRIES=(CI_REGISTRY QUAY_IO DOCKER_IO)
fi

export REGISTRY_AUTH_FILE=${HOME}/auth.json
for REGISTRY_VAR in "${REGISTRIES[@]}"; do
    if ! [ -v "${REGISTRY_VAR}" ]; then
        continue
    fi
    REGISTRY_PASSWORD_VAR="${REGISTRY_VAR}_PASSWORD"
    REGISTRY_USER_VAR="${REGISTRY_VAR}_USER"
    REGISTRY_IMAGE_VAR="${REGISTRY_VAR}_IMAGE"
    echo "${!REGISTRY_PASSWORD_VAR}" | skopeo login -u "${!REGISTRY_USER_VAR}" --password-stdin "${!REGISTRY_VAR}"
    REGISTRY_TAG=${!REGISTRY_IMAGE_VAR}/${IMAGE}

    if [ "${#IMAGE_EXTRA_TAGS[@]}" -gt 0 ]; then
        for EXTRA_TAG in "${IMAGE_EXTRA_TAGS[@]}"; do
            EXTRA_REGISTRY_TAG="${REGISTRY_TAG%:*}:${EXTRA_TAG}${IMAGE_ARCH:+-${IMAGE_ARCH}}"
            echo_yellow "tagging ${REGISTRY_TAG} as ${EXTRA_REGISTRY_TAG}"
            loop skopeo copy --all "docker://${REGISTRY_TAG}" "docker://${EXTRA_REGISTRY_TAG}"
        done
    elif [ -v IMAGE_ARCHES ]; then
        IFS=' ' read -r -a IMAGE_ARCHES_ARRAY <<< "${IMAGE_ARCHES}"
        # buildah manifest create cannot pull from private repos
        for IMAGE_ARCH in "${IMAGE_ARCHES_ARRAY[@]}"; do
            echo_yellow "pulling ${REGISTRY_TAG}-$IMAGE_ARCH"
            loop buildah pull "docker://${REGISTRY_TAG}-$IMAGE_ARCH"
        done
        buildah manifest create "${REGISTRY_TAG}" "${IMAGE_ARCHES_ARRAY[@]/#/containers-storage:${REGISTRY_TAG}-}"
        # quay.io somehow downgrades manifests and needs to be forced to v2s2
        echo_yellow "pushing ${REGISTRY_TAG}"
        loop buildah manifest push --format v2s2 --all "${REGISTRY_TAG}" "docker://${REGISTRY_TAG}"
    else
        echo_yellow "pushing to ${REGISTRY_TAG}"
        loop buildah push "${IMAGE}" "docker://${REGISTRY_TAG}"
    fi
done

if [ "${FAILED}" -gt 0 ]; then
    echo_red "$FAILED registry pushes failed."
    exit 1
fi
